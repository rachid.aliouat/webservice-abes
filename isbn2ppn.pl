#!c:\perl\bin  
#################
#date : 29/04/2016
#
# Questionne le webservice du sudoc ISBN2PPN sur la base d'un fichier d'ISBN : 1 ISBN par ligne
#################
#use strict; 
#use warnings; 

use LWP::Simple;
use XML::LibXML;

# lecture du fichier de configuration config.conf
open (CONF, "config.conf") or die "Ouverture fichier de configuration config.conf impossible , cause : $! \n";
print "lecture de la configuration:\n";
print "-" x 20, "\n";
@tab_inter = <CONF>; 
close(CONF);
for ($i=0;$i<=$#tab_inter;$i++) {
@ligne=split('=',$tab_inter[$i]); # decoupage ligne
chomp($ligne[1]);
$hash_conf{$ligne[0]}=$ligne[1]; # alimentation de la table de hash avec la conf
if ($ligne[0]!~ /pass/) {
	print $ligne[0] ,"->",$hash_conf{$ligne[0]} ,"\n";
	} else
	{
	print "$ligne[0]"."->"."*****"."\n";
	}

}
print "-" x 20, "\n";
######## Lire le fichier des ISBN

if ($#ARGV <0) # il n'y a pas d'argument fichier
	{
		print "Il manque le nom du fichier d'ISBN en parametre.";
		exit();
	}
	else  #OK
	{
	$fic_isbn=$ARGV[0];
	}
	
	
## Fichier rapport
open (RAPPORT, ">rapport.csv") or die "Ouverture fichier de configuration rapport.csv impossible , cause : $! \n";
### Fichier ISBN
open (ISBN, $fic_isbn) or die "Ouverture fichier de configuration $fic_isbn impossible , cause : $! \n";
@tab_inter = <ISBN>; 
close(ISBN);

$total_ppn=$#tab_inter +1;
$total_sans_ppn=0;
$total_avec_ppn=0;
print RAPPORT "ISBN\tPPN\tPPN-NH\n";
for ($i=0;$i<=$#tab_inter;$i++) {
	#chomp($tab_inter[$i]); # enlever le CR-LF
	#$tab_inter[$i]=~ s/\s//g;#enlever les espaces qui traineraient
	$ligne= epure($tab_inter[$i])."\t";
	my $URL = epure($hash_conf{url_webservice_isbn}).epure($tab_inter[$i]);
	print $URL,"\n";
	print "------REQUETE webservice sur ISBN $tab_inter[$i]\n";
	my $data = get($URL);
	
	print $data,"\n";
	if ($data!~ /<ppn>/) { ## pas de PPN retourné
	print "-->pas de ppn retourne.\n";
	$ligne.= "\t\n";
	$total_sans_ppn++;
	print RAPPORT $ligne;
	} else
	{
#### creer un objet XMl LibXML
	$total_avec_ppn++;
	print "--------Analyse XML----\n";
	my $parser     = XML::LibXML->new();
	my $tree = $parser->parse_string($data);
# Racine du document XML
	$root = $tree->getDocumentElement;
	my @query = $root->getElementsByTagName('result');
	foreach my $result (@query) { # Lit tous les 'ppn' sous la balise 'result'
		foreach my $ppn ( $result->getElementsByTagName('ppn') ) {
			print "ppn -->".$ppn->getFirstChild->getData, "\n";
			$ligne.= $ppn->getFirstChild->getData . ",";
		}
	}
	
	$ligne.= "\t";
	@query = $root->getElementsByTagName('resultNoHolding');
	foreach my $resultNh (@query) { # Lit tous les 'ppn' sous la balise 'result'
		foreach  my $ppn ( $resultNh->getElementsByTagName('ppn') ) {
			print "ppn NH -->".$ppn->getFirstChild->getData, "\n";
			$ligne.= $ppn->getFirstChild->getData . ",";
		}
	}
	$ligne.="\n";
	### supprimer les virgule inutile ",tabulation" devient "tabulation"
	$ligne =~ s/,\t/\t/;
	$ligne =~ s/,\n/\n/;
	print RAPPORT $ligne;
}
}
close (RAPPORT);
print "===================\n";
$taux_recouvrement=($total_ppn - $total_sans_ppn)*100/$total_ppn;
print "Nombre d'ISBN recherches : $total_ppn\n";
print "Nombre d'ISBN trouves : $total_avec_ppn\n";
print "Nombre d'ISBN non trouves : $total_sans_ppn\n";
print "Taux de recouvrement : ".int($taux_recouvrement),"%\n";




########### fonction Retourne la chaine de caractère passée en paramètre en ne laissant que les caractères autorisés
sub epure { 
	my ($res)=@_; 
	
	$res=~ s/\n//g;
	$res=~ s/\r//g;
	$res=~ s/\"//g;
	$res=~ s/\s//g;
	
	return $res;
}
