#!c:\perl\bin  
#################
#date : 10/03/2017
#
# Questionne le webservice du sudoc Multiwhere sur la base d'un fichier de PPN : 1 PPN par ligne
# si le PPN est inconnu, il questionne le webservice Merged et s'il y a un PPN retourné, il indique "Fusion" à la place d'inconnu
###################
# doc--> http://documentation.abes.fr/sudoc/manuels/administration/aidewebservices/multiwhere.html
#Un service Sudoc pour localiser (RCR de localisation) plusieurs documents à partir de leur identifiant (PPN)
#Un service where a été précédemment développé pour permettre la localisation d’un seul document à partir de son identifiant (PPN).
#Il est dorénavant conseillé d’utiliser le multiwhere qui offre un service plus complet et plus riche que le where.
#Les notices de RCR ont été enrichies des informations de géolocalisation : latitude et longitude.
# doc--> http://documentation.abes.fr/sudoc/manuels/administration/aidewebservices/mergedppn.html
# A partir d'un N° source (notice fusionnée ) : trouver le PPN actif (notice valide)
# Ce service permet de retrouver le ppn de la notice bibliographique conservée à partir du ppn de la notice bibliographique supprimée.
#############
#En Entrée :
#une liste de PPN
#une liste de RCR
#
#En sortie :
#un rapport csv séparateur tabulation indiquant pour chaque PPN :
#trouvé ou non dans le Sudoc
#pour chaque PPN trouvé , la liste des localisations correspodants aux RCR données
#################
#use strict; 
#use warnings; 

use LWP::Simple;
use XML::LibXML;
%hash_conf=();
%hash_rcr=();
# lecture du fichier de configuration config.conf
open (CONF, "config.conf") or die "Ouverture fichier de configuration config.conf impossible , cause : $! \n";
print "lecture de la configuration:\n";
print "-" x 20, "\n";
@tab_inter = <CONF>; 
close(CONF);
for ($i=0;$i<=$#tab_inter;$i++) {
@ligne=split('=',$tab_inter[$i]); # decoupage ligne
chomp($ligne[1]);
$hash_conf{$ligne[0]}=$ligne[1]; # alimentation de la table de hash avec la conf
if ($ligne[0]!~ /pass/) {
	print $ligne[0] ,"->",$hash_conf{$ligne[0]} ,"\n";
	} else
	{
	print "$ligne[0]"."->"."*****"."\n";
	}

}
print "-" x 20, "\n";
######## Lire le fichier des PPN

if ($#ARGV <1) # il n'y a pas d'argument fichier
	{
		print "Il manque le nom du fichier de PPN en parametre et/ou le fichier de RCR.";
		exit();
	}
	else  #OK
	{
	$fic_ppn=$ARGV[0];
	$fic_rcr=$ARGV[1];
	}
	
	
## Fichier rapport
open (RAPPORT, ">rapport.csv") or die "Ouverture fichier de configuration rapport.csv impossible , cause : $! \n";
### fichier des RCR
open (RCR, $fic_rcr) or die "Ouverture fichier de configuration $fic_rcr impossible , cause : $! \n";
@tab_inter = <RCR>; 
close(RCR);
for ($i=0;$i<=$#tab_inter;$i++) {
	#chomp($tab_inter[$i]); # enlever le CR-LF
	#$tab_inter[$i]=~ s/\s//g;#enlever les espaces qui traineraient
	$hash_rcr{epure($tab_inter[$i])}=epure($tab_inter[$i]); # alimentation de la table de hash avec les rcr
	}
### Fichier PPN
open (PPN, $fic_ppn) or die "Ouverture fichier de configuration $fic_ppn impossible , cause : $! \n";
@tab_inter = <PPN>; 
close(PPN);
$total_ppn=$#tab_inter +1 ;
$total_sans_ppn=0;
$total_avec_ppn=0;
$total_avec_fusion=0;

print RAPPORT "PPN-source\tPPN-VALIDE\tRCR-LOC\n";
for ($i=0;$i<=$#tab_inter;$i++) {
	#chomp($tab_inter[$i]); # enlever le CR-LF
	#$tab_inter[$i]=~ s/\s//g;#enlever les espaces qui traineraient
	$tab_inter[$i]=~ s/PPN//g;#enlever le PPN qui traine
	$ligne= epure($tab_inter[$i])."\t";
	my $URL = epure($hash_conf{url_webservice_multiwhere}).epure($tab_inter[$i]);
	print "------REQUETE webservice sur PPN $tab_inter[$i]\n";
	my $data = get($URL);
	#print $data,"\n";
	if ($data!~ /<sudoc/) { ## PPN inconnu
		print "-->PPN inconnu.\n";
		print "-->requete du webservice Merged.\n";
		my $URLM = $hash_conf{url_webservice_merged}.$tab_inter[$i];
		my $dataM = get($URLM);
		if ($dataM!~ /<ppn>/) { ## pas de PPN retourné
			$ligne.= "inconnu\t";
			$total_sans_ppn++;
		} else
		{
		$ligne.= "fusion\t";
		$total_avec_fusion++;
		$total_avec_ppn++;
		}
	} else
	{
#### creer un objet XMl LibXML
	$ligne.= "OK\t";
	$total_avec_ppn++;
	print "--------Analyse XML----\n";
	my $parser     = XML::LibXML->new();
	my $tree = $parser->parse_string($data);
# Racine du document XML
	$root = $tree->getDocumentElement;
	my @query = $root->getElementsByTagName('result');
	foreach my $result (@query) { # Lit tous les 'rcr' sous la balise 'result'
		foreach my $ppn ( $result->getElementsByTagName('rcr') ) {
			if($hash_rcr{$ppn->getFirstChild->getData}){ # si le RCR est dans la liste
			print "rcr -->".$ppn->getFirstChild->getData, "\n";
			$ligne.= $ppn->getFirstChild->getData . ",";
			}
		}
	}
	
	
	}
	$ligne.="\n";
	### supprimer les virgule inutile ",tabulation" devient "tabulation"
	$ligne =~ s/,\t/\t/;
	$ligne =~ s/,\n/\n/;
	print RAPPORT $ligne;
}

close (RAPPORT);
print "===================\n";
#print "($total_ppn - $total_sans_ppn)*100/$total_ppn\n";
$taux_recouvrement=($total_ppn - $total_sans_ppn)*100/$total_ppn;
print "Nombre de PPN recherches : \t\t$total_ppn\n";
print "Nombre de PPN trouves : \t\t$total_avec_ppn\n";
print "\t dont PPN fusionnes : \t$total_avec_fusion\n";
print "Nombre de PPN non trouves : \t\t$total_sans_ppn\n";
print "Taux de recouvrement : \t\t".int($taux_recouvrement),"%\n";



########### fonction Retourne la chaine de caractère passée en paramètre en ne laissant que les caractères autorisés
sub epure { 
	my ($res)=@_; 
	
	$res=~ s/\n//g;
	$res=~ s/\r//g;
	$res=~ s/\"//g;
	$res=~ s/\s//g;
	
	return $res;
}
