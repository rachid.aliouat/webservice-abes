#!c:\perl\bin  

use strict; 
use warnings; 

use LWP::Simple;
#use XML::LibXML;
use XML::Twig;
use XML::LibXML;

my $data = get("http://www.sudoc.fr/services/isbn2ppn/2-07-037026-7889");
   
print "Recuperation de " . length($data) . " octets.\n";
print "------------\n";
print $data,"\n";
print "------------\n";

######TEST AVEC TWIG
#### creer un objet XMl twig
print "------------\n";
print "--------AVEC TWIG----\n";
my $twig= XML::Twig->new();
#### parser le contenu de $data avec Twig
$twig->parse($data);

my $root = $twig->root;
# Chaque result
foreach my $TwigResult ( $root->children('query') ) {
	print "sudoc->isbn =>".$TwigResult->field('isbn')."\n";
	print "sudoc->result =>".$TwigResult->field('result')."\n";
	print "sudoc->resultNoHolding =>".$TwigResult->field('resultNoHolding')."\n";
	foreach my $TwigPpn ( $TwigResult->children('result') ) {
		print "result->ppn =>".$TwigPpn->field('ppn')."\n";
	}
	foreach my $TwigPpn ( $TwigResult->children('resultNoHolding') ) {
		print "resultNoHolding->ppn =>".$TwigPpn->field('ppn')."\n";
	}
}

######TEST AVEC LIBXML
#### creer un objet XMl LibXML
print "------------\n";
print "--------AVEC LIBXML----\n";
my $parser     = XML::LibXML->new();
my $tree = $parser->parse_string($data);
# Racine du document XML
$root = $tree->getDocumentElement;
my @query = $root->getElementsByTagName('result');
foreach my $result (@query) { # Lit tous les 'ppn' sous la balise 'result'
	foreach my $ppn ( $result->getElementsByTagName('ppn') ) {
		print "ppn -->".$ppn->getFirstChild->getData, "\n";
	}
}
@query = $root->getElementsByTagName('resultNoHolding');
foreach my $resultNh (@query) { # Lit tous les 'ppn' sous la balise 'result'
	foreach  my $ppn ( $resultNh->getElementsByTagName('ppn') ) {
		print "ppn NH -->".$ppn->getFirstChild->getData, "\n";
	}
}
